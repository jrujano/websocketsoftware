var d3 = require("d3");
import * as lfg from "./liquidFillGauge_last";
var test_id="chart";
if (lfg){
    var config=lfg.liquidFillGaugeDefaultSettings();
    config.circleThickness=0.15;
    config.circleColor="#808015";
    config.textColor="#555500";
    config.waveTextColor="#FFFFAA";
    config.waveColor="#AAAA39";
    config.textVertPosition=0.8;
    config.waveAnimateTime=1000;
    config.waveHeight=0.05;
    config.waveAnimate=true;
    config.waveRise=false;
    config.waveHeightScaling=false;
    config.waveOffset=0.25;
    config.textSize=0.75;
    config.waveCount=3;
    var gauge=lfg.loadLiquidFillGauge("chart",40,config);}
