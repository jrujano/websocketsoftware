import moment from 'moment';
import {gauge_tpl, fuel_gauge_tpl, time_tpl,
        load_template, give_values, tpl_gauge_opts, tpl_time_opts} from './templates';
import {create_gauge,create_fuel_gauge, load_chart} from "./charts";
import {gauge_charts, fuel_gauge_charts} from '/charts';

function get_last_element(data, list){
    let result = data;
    for (let index in list){
        let elem = list[index];
        if ( result.hasOwnProperty(elem)){
            result = result[elem];
        }
        else{
            result = "";
        }

    }
    return result;
};


function findPosition(list, text){
    for(let e in list){
        let elem = list[e];
        if (elem.textContent==text){
            return e;
        }
    }
};

function add_row2table(table, table_head, key_name, data, position, css_id_class, other_opts){
    let headers=table_head.getElementsByTagName("tr")[0].getElementsByTagName("th");
    let row = table.insertRow(position);
    Object.entries(css_id_class).forEach((key, value)=>{
        row.setAttribute(...key);
    });
    let code=other_opts['code'];
    let template=other_opts['tpl'];
    let with_tpl=other_opts['with_tpl'];
    let width=other_opts.size.w;
    let height=other_opts.size.h;
    for (let item=0; item<headers.length;item++){
        let item_name = headers[item].textContent;
        let elem = get_last_element(data, key_name[item_name]);
        let cell = row.insertCell(item);
        let last_name=key_name[item_name].slice(-1)[0];
        cell.setAttribute("class",last_name);
        if (item_name in with_tpl){
            switch(with_tpl[item_name]["tipo"]){
            case "gauge":
                let tpl_opts_json=tpl_gauge_opts(code+"_"+last_name, width, height);
                let inner_html=give_values(template['gauge'], tpl_opts_json);
                // look for class gauge_status--->svg-->id
                //
                cell.innerHTML=inner_html;
                let svg=cell.getElementsByTagName("svg")[0];
                let id_svg=svg.id;
                // code, type:last_name, id_svg,
                // create chart
                // here->other_opts['chart_opts'][column]
                let gauge_params=other_opts['chart_opts'][item_name];
                let new_gauge=create_gauge(id_svg, gauge_params, 1);
                let chart_map=other_opts["charts"]['liquid_gauge'];
                // load _chart
                load_chart(code, last_name, id_svg, new_gauge, chart_map);
                break;;
            case 'fuel_gauge':
                let tpl_fuel_opts_json=tpl_time_opts(code+"_TO", width, height);
                let inner_fuel_html=give_values(template['fuel_gauge'], tpl_fuel_opts_json);
                cell.innerHTML=inner_fuel_html;
                let svg_fuel=cell.getElementsByTagName("svg")[0];
                let id_svg_fuel=svg_fuel.id;
                // code, type:last_name, id_svg,
                // create chart
                // here->other_opts['chart_opts'][column]
                let fuel_gauge_params=other_opts['chart_opts'][item_name];
                let new_fuel_gauge=create_fuel_gauge(id_svg_fuel, fuel_gauge_params);
                let fuel_chart_map=other_opts["charts"]['fuel_gauge'];
                // load _chart
                load_chart(code, last_name, id_svg_fuel, new_fuel_gauge, fuel_chart_map);
                new_fuel_gauge.render(200);
                break;;
            case "datetime":
                let tpl_time_opts_json=tpl_time_opts(code);
                let inner_time_html=give_values(template['datetime'], tpl_time_opts_json);
                cell.innerHTML=inner_time_html;
                break;;
            }
        }
        else{
            cell.innerHTML=elem;};
    }
}

function print(...value){
    console.log(...value);
}

function complete_table(table_name, key_name, data_list, table_map, other_opts){
    let table=document.getElementById(table_name).getElementsByTagName('tbody')[0];
    let table_head=document.getElementById(table_name).getElementsByTagName('thead')[0];
    //let table_head=thead.getElementsByTagName("tr")[0].getElementsByTagName("th");
    let class_style="status_row";
    let css_id_class ={'class':class_style};
    // read template
    let tpl_path=other_opts['tpl_path'];
    let tpl_file=load_template(tpl_path);
    other_opts['tpl']=tpl_file;
    for (let data_pos in data_list){
        let position=parseInt(data_pos);
        let data=data_list[data_pos];
        let code=data['ids'];
        let id_style=code+"_row";
        css_id_class['id']=id_style;
        table_map[code]=position;
        other_opts['code']=code;
        add_row2table(table, table_head, key_name, data, position, css_id_class, other_opts);
    }
}

function update_gauge_value(cell, id_css, code, item, value){
    // if id_css==value
    let cell_value = cell.getElementsByClassName(id_css)[0];
     if (id_css=='gauge_status'){
         let gauge=gauge_charts[code][item]['object'];
         gauge.update(value);
    }
    else{
        cell_value.innerHTML=value;
    };
}

function update_fuel_gauge_value(cell, id_css, code, item, value){
    // if id_css==value
    let cell_value = cell.getElementsByClassName(id_css)[0];
    if (id_css=='gauge_time_on'){
        let gauge=fuel_gauge_charts[code][item]['object'];
        gauge.redraw(value*0.3);
    }
    else{
        cell_value.innerHTML=value;
    };
}



function update_table(data_gen, table_name, headers, table_map, column_map, other_opts){
    let table=document.getElementById(table_name).getElementsByTagName('tbody')[0];
    let code=data_gen['station'];
    if (code=='VALN'){
        console.log("Data VALN", data_gen);
    };
    let with_tpl=other_opts['with_tpl'];
    if (table_map.hasOwnProperty(code)){
      let position=table_map[code];
      for(let column in headers){
          let item_list=headers[column];
          let column_position=column_map[column];
          let value=get_last_element(data_gen, item_list);
          let cell =table.rows[position].cells[column_position];
          if (value && table.rows[position].cells[column_position]){
              // item_name -> , with_tpl, template (NO), cell
              if (column in with_tpl){
                  /*set value innerHTML on #id value*/
                  let settings=with_tpl[column];
                  update_gauge_value(cell, 'value', code, item_list[item_list.length-1],value);
                  switch (settings['tipo']){
                  case 'gauge':
                      update_gauge_value(cell, 'gauge_status', code, item_list[item_list.length-1], value);
                      break;;
                  case 'fuel_gauge':
                      update_fuel_gauge_value(
                          cell,
                          'gauge_time_on',
                          code,
                          item_list[item_list.length-1],
                          value);
                      break;;
                  case 'datetime':
                      let cell_value = cell.getElementsByClassName("datetime")[0];
                      let date_cell = document.getElementById("column-date");
                      let dt=new Date(value*1000);
                      cell_value.innerHTML=moment(dt).format('HH:mm:ss');
                      date_cell.innerHTML=moment(dt).format("DD/MM/YYYY");
                      break;;
                  }

              }
              else{
                  cell.innerHTML=value;};
          }
      }
    }
}


function deltatime(last_time, row, position, code){
    let now=new Date().getTime();
    let ts=last_time;
    if (ts){
        let delta=now/1000-parseInt(ts);
        let nf = Intl.NumberFormat();
        //row.cells[position+1].innerHTML=nf.format(delta);
        let cell=row.cells[position+1];
        update_fuel_gauge_value(
                          cell,
                          'gauge_time_on',
                          code,
                          "to",
                          delta);
        // rescue gauge
        return delta;
    }
};

function update_field(table_name, field, callback, callback_args, table_map, column_map){
    let table=document.getElementById(table_name).getElementsByTagName('tbody')[0];
    let field_index=column_map[field];
    for (let value in table_map){
        // obtener valor en campo
        let index=table_map[value];
        let row = table.rows[index];
        if(row.cells[field_index]){
            let field_value = row.cells[field_index].textContent;
            let result = callback(field_value, row, field_index, value, ...callback_args);}
    };
};

/*
  Define client for use in node
*/


export {get_last_element,
        findPosition,
        add_row2table,
        print,
        complete_table,
        update_gauge_value,
        update_fuel_gauge_value,
        update_table,
        deltatime,
        update_field}
