let WebSocketServer = require('websocket').server;
let WebSocketClient = require('websocket').client;
let WebSocketFrame  = require('websocket').frame;
let WebSocketRouter = require('websocket').router;
let W3CWebSocket = require('websocket').w3cwebsocket;
import {CODIGO,NOMBRE,BATERIA,MEMORIA,ULTIMO_DATO,TPO_OFFLINE, DOP} from './names';
import {gauge_tpl, fuel_gauge_tpl, time_tpl,
        load_template, give_values, tpl_gauge_opts, tpl_time_opts} from './templates';
import {get_last_element,
        findPosition,
        add_row2table,
        print,
        complete_table,
        update_gauge_value,
        update_fuel_gauge_value,
        update_table,
        deltatime,
        update_field} from './table_manage';
import {gauge_charts, fuel_gauge_charts} from '/charts';
function build_uri(ws_data){
    let uri="ws://"+ws_data.ip+":"+ws_data.port+"/"+ws_data.path;
    return uri;
};
let ws_data=document.getElementById("websocket_data_json").textContent;
let ws_data_json = JSON.parse(ws_data);
let uri = build_uri(ws_data_json);
function create_station_list(station_list){
if (station_list.length==0){
station_list = [
    {type:'rtx', pos:{lat:-25.36,lon:-71.361}, ids:"valn"},
    {type:'no_rtx', pos:{lat:-26.36,lon:-71.561}, ids:"trpd"},
    {type:'rtx', pos:{lat:-28.36,lon:-71.361}, ids:"qtay"},
    {type:'rtx', pos:{lat:-30.36,lon:-70.51}, ids:"ccsn"},
    {type:'no_csn', pos:{lat:-41.36,lon:-70.61}, ids:"atjn"},
    {type:'no_rtx', pos:{lat:-38.36,lon:-70.361}, ids:"bing"}
];
    return station_list;
}
    else{return station_list;}
}
let json_data=document.getElementById("station_data_list").textContent;
let station_list = [];
if (json_data.trim().length>0){
    station_list=JSON.parse(json_data);
}
station_list = create_station_list(station_list);
let key_name_json ={
    [CODIGO]:["ids"],
    [NOMBRE]:["station"],
    [BATERIA]:["data","BATT_MEM","BATT_CAPACITY"],
    [MEMORIA]:["data","BATT_MEM","REMAINING_MEM"],
    [ULTIMO_DATO]:["data","BATT_MEM","TIMESTAMP"],
    [TPO_OFFLINE]:["to"],
    [DOP]:["dop"]
};

let column_map ={
    [CODIGO]:0,
    [NOMBRE]:1,
    [BATERIA]:2,
    [MEMORIA]:3,
    [ULTIMO_DATO]:4,
    [TPO_OFFLINE]:5,
    [DOP]:6
};

/*
funciones de operacion sobre valores
*/
function simple(value){
    return value;
};

function porcentaje_mem(value, base){
    return value;
};

function ts2datetime(value){
    let datetime=new Date(value*1000);
    return datetime;
}

function time_online(value){
    return value;
}
/**/
let table_map={};
let other_opts={
    tpl_path:{
        'gauge':gauge_tpl,
        'fuel_gauge':fuel_gauge_tpl,
        'datetime': time_tpl},
    size:{'w':80,'h':80},
    with_tpl:{[BATERIA]:{tipo:'gauge',op:simple,"op_args":[]},
              [MEMORIA]:{tipo:'gauge',op:porcentaje_mem,"op_args":[]},
              [ULTIMO_DATO]:{tipo:'datetime',op:ts2datetime},"op_args":[],
              [TPO_OFFLINE]:{tipo:'fuel_gauge',op:time_online,op_args:[]}},
    charts:{'liquid_gauge':gauge_charts, 'fuel_gauge': fuel_gauge_charts},
    chart_opts:{
        [BATERIA]:{
            circleThickness:0.15,
            textVertPosition:0.8,
            waveAnimate:true,
            textSize:0.75,
            waveCount:3,
            waveAnimateTime:1000,
            textSize:.8,
        },
        [MEMORIA]:{
            circleThickness:0.15,
            textVertPosition:0.8,
            waveAnimate:true,
            textSize:.8,
            minValue:0,
            maxValue:4000,
            waveAnimateTime:1000,
            waveCount:5,
            circleColor:"#F4D03F",
            waveColor:"#F5B041",
            textTextColor:"#229954",
            waveTextColor:"#229954"
        },
        [TPO_OFFLINE]:{
            size: 100,
            minorTicks: 1,
            showPointer: true,
            showValue: true,
            valueFontSize: 16,
            unit: 's',
            redZone: {
                from: 80,
                to: 120
            },
            yellowZone: {
                from: 40,
                to: 80
            },
            greenZone: {
                from: 0,
                to: 40
            }
        }
    }
};
/*
Time checking
  Function W3C Standar WS Client
 */
function w3c_websocket_client(uri){
  let client = new W3CWebSocket(uri);

  client.onerror = function() {
      console.log('Connection Error');
  };

  client.onopen = function() {
      console.log('WebSocket Client Connected');
      complete_table('data_table', key_name_json, station_list, table_map, other_opts);
      /*
      function sendNumber() {
          if (client.readyState === client.OPEN) {
              let number = Math.round(Math.random() * 0xFFFFFF);
              client.send(number.toString());
              // repeat every second this msg ::::
              setTimeout(sendNumber, 1000);
          }
      }
      sendNumber();*/
  };

  client.onclose = function() {
      console.log('echo-protocol Client Closed');
  };

  client.onmessage = function(e) {
      if (typeof e.data === 'string') {
          update_table(JSON.parse(e.data),'data_table', key_name_json, table_map, column_map, other_opts );
      }
      //console.log("GAUGE_CHARTS", gauge_charts);
  };
};
/*
  Define URL
*/
let kwargs = {
    'delta_time': 2,
    'port': 8000,
    'path': 'ws/status_all/ws_users/'};
/*
init js script
 */
let uri_top ="ws://localhost:"+kwargs['port']+"/"+kwargs['path'];
console.log("The URI is :", uri_top);
let init_time_on=update_field("data_table", ULTIMO_DATO, deltatime, [], table_map, column_map);
setInterval(
    function(){
        update_field("data_table", ULTIMO_DATO, deltatime, [], table_map, column_map);
    },1000);
w3c_websocket_client(uri);
