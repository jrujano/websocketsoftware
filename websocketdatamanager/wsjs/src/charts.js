let d3 = require("d3");
import * as lfg from "./liquidFillGauge_last";
import * as fuel_gauge from './fuel_gauge';


function create_gauge(chart_id, gauge_params, initial_value){
    let config=lfg.liquidFillGaugeDefaultSettings();
    if (gauge_params){
        let gp=gauge_params;
        Object.keys(config).forEach( key =>{
            if (gp.hasOwnProperty(key))
            {
                config[key]=gp[key];
            }
        }
                                   );
    };
    let gauge=lfg.loadLiquidFillGauge(chart_id,initial_value,config);
    return gauge;
};

function create_fuel_gauge(chart_id, gauge_params){
    let config={};
    if (gauge_params){
        let gp=gauge_params;
        Object.keys(config).forEach( key =>{
            if (gp.hasOwnProperty(key))
            {
                config[key]=gp[key];
            }
        }
                                   );
    };
    let gauge=new fuel_gauge.Gauge("#"+chart_id, gauge_params);
    return gauge;
};

let gauge_charts = {
};

let fuel_gauge_charts = {
};

function load_chart(code, type, idc, chart, chart_map){
    let item_chart = {id:idc, object:chart};
    if (!chart_map.hasOwnProperty(code)){
        chart_map[code]={};}
    chart_map[code][type]=item_chart;
}


export {create_gauge, create_fuel_gauge, load_chart, gauge_charts, fuel_gauge_charts}
