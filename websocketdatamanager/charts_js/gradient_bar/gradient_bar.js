// Article: https://dev.to/chooblarin/d3js-chart-with-gradient-color-4j71

const margin = { top: 20, right: 20, bottom: 160, left: 45 }
const svgWidth = 420;
const svgHeight = 320;
const width = svgWidth - margin.left - margin.right
const height = svgHeight - margin.top - margin.bottom

const svg = d3
  .select('#barGradient')
  .append('svg')
  .attr('width', svgWidth)
  .attr('height', svgHeight);

const graphArea = svg
  .append('g')
  .attr('transform', `translate(${margin.left}, ${margin.top})`);

// Donloaded from https://www.imdb.com/list/ls021348496/
const dataURL = './imdb.csv';

d3.csv(dataURL).then(function(data) {
  console.log(data); // [{"Hello": "world"}, …]
});

(async () => {
  let data = await d3.csv(dataURL);

  const getX = d => d["Title"];
  const getY = d => +d["Num Votes"]; // convert to number from string

  // filter Top 10 data, and sort by num of votes
  data = data
    .slice(0, 10)
    .sort( (a, b) => -(getY(a) - getY(b)));

  const x = d3.scaleBand()
    .rangeRound([0, width])
    .domain(data.map(getX))
    .padding(0.1);

  const y = d3.scaleLinear()
    .range([height, 0])
    .domain([
      d3.min(data, getY),
      d3.max(data, getY)
    ])
    .nice();

  const defs = svg.append('defs');

  const bgGradient = defs
    .append('linearGradient')
    .attr('id', 'bg-gradient')
    // .attr('x1', '0')
    // .attr('y1', '0')
    // .attr('x2', '0')
    // .attr('y2', '1')
    .attr('gradientTransform', 'rotate(90)');

  bgGradient
    .append('stop')
    .attr('stop-color', '#F2C66B')
    .attr('offset', '0%');
  bgGradient
    .append('stop')
    .attr('stop-color', '#D13D73')
    .attr('offset', '100%');

  defs
    .append('clipPath')
    .attr('id', 'clip-bar-rects')
    .selectAll('bar')
    .data(data)
    .enter()
    .append('rect')
    .attr("x", d => x(getX(d)))
    .attr("y", d => y(getY(d)))
    .attr("width", x.bandwidth())
    .attr("height", d => height - y(getY(d)));

  const clipPath = graphArea
    .append('g')
    .attr('clip-path', `url(#clip-bar-rects)`);

  clipPath
    .append('rect')
    .attr('x', 0)
    .attr('y', 0)
    .attr('width', width)
    .attr('height', height)
    .style('fill', 'url(#bg-gradient)');

  const xAxis = d3.axisBottom(x);
  const yAxis = d3.axisLeft(y).ticks(5);

  const ax = graphArea
    .append('g')
    .attr('class', 'axis')
    .attr('transform', `translate(0, ${height})`)
    .call(xAxis);

  ax.selectAll('text')
    .style("text-anchor", "start")
    .style("alignment-baseline", "middle")
    .attr("transform", `translate(${x.bandwidth() / 2}, 10) rotate(90)`)

  graphArea
    .append('g')
    .attr('class', 'axis')
    .call(yAxis);

})();
