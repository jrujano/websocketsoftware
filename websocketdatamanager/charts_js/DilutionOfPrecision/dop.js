/* Radar chart design created by Nadieh Bremer - VisualCinnamon.com 

const HDOP="HDOP";
const VDOP="VDOP";
const PDOP="PDOP";
const TDOP="TDOP";
const GDOP="GDOP";

////////////////////////////////////////////////////////////// 
//////////////////////// Set-Up ////////////////////////////// 
//////////////////////////////////////////////////////////////

let margin = {top: 100, right: 100, bottom: 100, left: 100};
let  width = 70; // Math.min(700, window.innerWidth - 10) - margin.left - margin.right;
let  height = 70; //Math.min(width, window.innerHeight - margin.top - margin.bottom - 20);


////////////////////////////////////////////////////////////// 
////////////////////////// Data ////////////////////////////// 
  //////////////////////////////////////////////////////////////
var data = [
    {key:"Dilution of Presicion",
     value:[
         {axis:HDOP,value:0.22},
         {axis:VDOP,value:0.28},
         {axis:TDOP,value:0.29},
         {axis:PDOP,value:0.45},
         {axis:GDOP,value:.99},]
    }
    ];
////////////////////////////////////////////////////////////// 
//////////////////// Draw the Chart ////////////////////////// 
////////////////////////////////////////////////////////////// 
var color = d3.scaleOrdinal()
  .range(["#EDC951","#CC333F","#00A0B0"]);

var radarChartOptions = {
  w: width,
  h: height,
  margin: margin,
  maxValue: 1,
  levels: 6,
  roundStrokes: true,
    color: color,
    format:'.0%',
};
//Call function to draw the Radar chart
let radar=RadarChart();
d3.select(".radarChart").call(radar)
console.log("Radar->", radar);
radar.data(data).options(radarChartOptions).update();
console.log("Radar updated")
*/

var color = d3.scaleOrdinal()
  .range(["#EDC951","#CC333F","#00A0B0"]);

var radarChartOptions = {
     width: 600,
     height: 600,
    color: color,
    levels:6,
    circles:{maxValue:1}
};
   radarChart = RadarChart()
   d3.select('#radarChart')
     .call(radarChart);
radarChart.options(radarChartOptions).update();



   var operation = d3.select('body').append('div').append('h2');

   var data = 
      [  
        {  
          "key":"Nokia Smartphone",
          "values":[  
            {  "axis":"Battery Life", "value":0.26 }, {  "axis":"Brand", "value":0.10 },
            {  "axis":"Contract Cost", "value":0.30 }, {  "axis":"Design And Quality", "value":0.14 },
            {  "axis":"Have Internet Connectivity", "value":0.22 }, {  "axis":"Large Screen", "value":0.04 },
            {  "axis":"Price Of Device", "value":0.41 }, {  "axis":"To Be A Smartphone", "value":0.30 }
          ]
        },
        {  
          "key":"Samsung",
          "values":[  
            {  "axis":"Battery Life", "value":0.27 }, {  "axis":"Brand", "value":0.16 },
            {  "axis":"Contract Cost", "value":0.35 }, {  "axis":"Design And Quality", "value":0.13 },
            {  "axis":"Have Internet Connectivity", "value":0.20 }, {  "axis":"Large Screen", "value":0.13 },
            {  "axis":"Price Of Device", "value":0.35 }, {  "axis":"To Be A Smartphone", "value":0.38 }
          ]
        },
        {  
          "key":"iPhone",
          "values":[  
            {  "axis":"Battery Life", "value":0.22 }, {  "axis":"Brand", "value":0.28 },
            {  "axis":"Contract Cost", "value":0.29 }, {  "axis":"Design And Quality", "value":0.17 },
            {  "axis":"Have Internet Connectivity", "value":0.22 }, {  "axis":"Large Screen", "value":0.02 },
            {  "axis":"Price Of Device", "value":0.21 }, {  "axis":"To Be A Smartphone", "value":0.50 }
          ]
        }
      ];

console.log("Pre data", data);
radarChart.data(data).update();
radarChart.data(data).update();

//radarChart.options({'legend': {display: true}});
//radarChart.colors({'iPhone': 'blue', 'Samsung': 'red', 'Nokia Smartphone': 'yellow'}).update();
