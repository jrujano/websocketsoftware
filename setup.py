from setuptools import setup

setup(name='websocketdatamanager',
      version='0.1',
      description='Websocket data manager, from rmq to ws',
      url='https://gitlab.com/pineiden/websocketsoftware',
      author='David Pineda Osorio',
      author_email='dpineda@csn.uchile.cl',
      license='MIT',
      packages=['collector', 'networktools'],
      zip_safe=False)
